---@diagnostic disable: undefined-global

local ui_folder_path = "ui"

vim.api.nvim_create_autocmd("BufWritePost", {
    group = vim.api.nvim_create_augroup("UserCustom_YtmPlayerRepoUiCompile", {}),
    pattern = "*.ui",
    callback = function(args)
        local buf = args.buf
        local file = vim.api.nvim_buf_get_name(buf)

        local proj_root = vim.fs.dirname(vim.fs.find(".git", {
            upward = true,
            path = vim.fs.dirname(file),
        })[1])

        -- print(proj_root, " ", file)
        vim.system({ proj_root .. "/compile_ui.sh", proj_root, file })
    end,
})
