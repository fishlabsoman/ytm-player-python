#!/bin/sh

proj_root="$1"
file_path="$2"

find_uic() {
    uic=""
    if uic -g python "${file_path}" | grep -q "Qt User Interface Compiler version 6."; then
        uic="uic"
    elif [ -f "/usr/lib/qt6/uic" ] && /usr/lib/qt6/uic -g python "${file_path}" | grep -q "Qt User Interface Compiler version 6."; then
        uic="/usr/lib/qt6/uic"
    fi
}

find_uic
find "$proj_root" -name "$(basename "$file_path")" -exec "${uic}" -g python "${file_path}" -o "${file_path%%.ui}.py" \;
